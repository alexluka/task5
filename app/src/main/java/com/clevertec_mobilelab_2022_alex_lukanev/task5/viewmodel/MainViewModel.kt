package com.clevertec_mobilelab_2022_alex_lukanev.task5.viewmodel

import androidx.lifecycle.ViewModel
import com.clevertec_mobilelab_2022_alex_lukanev.task5.api.BelarusbankGomelPlacesApiImpl


class MainViewModel: ViewModel() {

    fun getAllBelarusbank() = BelarusbankGomelPlacesApiImpl.getBelarusbankGomelPlaces()
}