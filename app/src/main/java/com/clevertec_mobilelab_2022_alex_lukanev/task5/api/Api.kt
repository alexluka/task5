package com.clevertec_mobilelab_2022_alex_lukanev.task5.api

import com.clevertec_mobilelab_2022_alex_lukanev.task5.data.*
import io.reactivex.Flowable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET

interface BelarusbankGomelPlacesApi {

    @GET("/api/atm?city=Гомель")
    fun getGomelAtms(): Flowable<List<AtmInfoboxApiData>>

    @GET("/api/infobox?city=Гомель")
    fun getGomelInfoboxes(): Flowable<List<AtmInfoboxApiData>>

    @GET("/api/filials_info?city=Гомель")
    fun getGomelFilials(): Flowable<List<FilialApiData>>
}

object BelarusbankGomelPlacesApiImpl {
    private val retrofit = Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .baseUrl("https://belarusbank.by")
        .build()

    private val belarusbankPlacesService = retrofit.create(BelarusbankGomelPlacesApi::class.java)

    fun getBelarusbankGomelPlaces(): BelarusbankGomelPlacesApi = belarusbankPlacesService

}