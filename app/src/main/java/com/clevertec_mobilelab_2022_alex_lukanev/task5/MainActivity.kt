package com.clevertec_mobilelab_2022_alex_lukanev.task5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import com.clevertec_mobilelab_2022_alex_lukanev.task5.data.Place
import com.clevertec_mobilelab_2022_alex_lukanev.task5.databinding.ActivityMainBinding
import com.clevertec_mobilelab_2022_alex_lukanev.task5.viewmodel.MainViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlin.math.pow
import kotlin.math.sqrt

const val LOCATION_X = 52.425163
const val LOCATION_Y = 31.015039

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val mainViewModel: MainViewModel by viewModels()

    private var myCompositeDisposable: CompositeDisposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        myCompositeDisposable = CompositeDisposable()

        myCompositeDisposable?.add(
            Flowable.zip(mainViewModel.getAllBelarusbank().getGomelAtms(),
                mainViewModel.getAllBelarusbank().getGomelInfoboxes(),
                mainViewModel.getAllBelarusbank().getGomelFilials()
            ) { atms, infoboxes, filials ->
                val belarusbankGomel = mutableListOf<Place>()
                atms.forEach { item ->
                    belarusbankGomel.add(
                        Place(
                            this.resources.getString(R.string.atm),
                            item.addressType,
                            item.address,
                            item.house,
                            item.gpsX.toDouble(),
                            item.gpsY.toDouble(),
                            distance(item.gpsX.toDouble(), item.gpsY.toDouble())
                        )
                    )
                }
                infoboxes.forEach { item ->
                    belarusbankGomel.add(
                        Place(
                            this.resources.getString(R.string.infobox),
                            item.addressType,
                            item.address,
                            item.house,
                            item.gpsX.toDouble(),
                            item.gpsY.toDouble(),
                            distance(item.gpsX.toDouble(), item.gpsY.toDouble())
                        )
                    )
                }
                filials.forEach { item ->
                    belarusbankGomel.add(
                        Place(
                            this.resources.getString(R.string.filial),
                            item.streetType,
                            item.street,
                            item.homeNumber,
                            item.gpsX.toDouble(),
                            item.gpsY.toDouble(),
                            distance(item.gpsX.toDouble(), item.gpsY.toDouble())
                        )
                    )
                }
                belarusbankGomel.sortBy { t -> t.distance }
                belarusbankGomel.subList(0, 10)
            }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                    {
                        val mapFragment = supportFragmentManager.findFragmentById(
                            R.id.map
                        ) as? SupportMapFragment

                        mapFragment?.getMapAsync { googleMap ->
                            addMarkers(googleMap, it)
                        }
                    },
                    { t -> Toast.makeText(this, t.message, Toast.LENGTH_SHORT).show() }
                ))
        Thread.sleep(500)
    }

    private fun distance(gpsX: Double, gpsY: Double): Double {
        return sqrt((gpsX - LOCATION_X).pow(2.0) + (gpsY - LOCATION_Y).pow(2.0))
    }

    private fun addMarkers(googleMap: GoogleMap, belarusbankGomel: MutableList<Place>) {
        val gomelLatLng = LatLng(LOCATION_X, LOCATION_Y)
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(gomelLatLng, 15.0f))
        belarusbankGomel.forEach { bank ->
            googleMap.addMarker(
                MarkerOptions()
                    .title(bank.type)
                    .snippet("${bank.addressType}${bank.address} ${bank.house}")
                    .position(LatLng(bank.gpsX, bank.gpsY))
            )
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        myCompositeDisposable?.clear()
    }
}